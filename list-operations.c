#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
   struct Node* head = NULL;
    int choice, data, key, searchValue, newValue;

    do {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printf("Linked List: ");
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("1. Delete by Key\n");
                printf("2. Delete by Value\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);

                if (choice == 1) {
                    printf("Enter key to delete: ");
                    scanf("%d", &key);
                    deleteByKey(&head, key);
                } else if (choice == 2) {
                    printf("Enter value to delete: ");
                    scanf("%d", &data);
                    deleteByValue(&head, data);
                } else {
                    printf("Invalid choice!\n");
                }
                break;
            case 5:
                printf("Exiting...\n");
                break;
            default:
                printf("Invalid choice! Please enter a valid option.\n");
        }
    } while (choice != 5);

    // Free the memory allocated for the linked list
    struct Node* temp = head;
    while (temp != NULL) {
        struct Node* nextNode = temp->next;
        free(temp);
        temp = nextNode;
    }

    return 0;
}

// Questions:
// Implement the prototypes defined above. TODO

// Function to create a new node with given data
struct Node* createNode(int number) {
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = number;
    newNode->next = NULL;
    return newNode;
}

// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

// Function to print all nodes in the list
void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}


void deleteByKey(struct Node** head, int key) {
    struct Node* temp = *head;
    struct Node* prev = NULL;

    if (temp != NULL && temp->number == key) {
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->number != key) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) return;

    prev->next = temp->next;
    free(temp);
}

void deleteByValue(struct Node** head, int value) {
    struct Node* temp = *head;
    struct Node* prev = NULL;

    if (temp != NULL && temp->number == value) {
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->number != value) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) return;

    prev->next = temp->next;
    free(temp);
}

void insertAfterKey(struct Node** head, int key, int value) {
    struct Node* temp = *head;

    while (temp != NULL && temp->number != key) {
        temp = temp->next;
    }

    if (temp == NULL) return;

    struct Node* newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

void insertAfterValue(struct Node** head, int searchValue, int newValue) {
    struct Node* temp = *head;

    while (temp != NULL && temp->number != searchValue) {
        temp = temp->next;
    }

    if (temp == NULL) return;

    struct Node* newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}

// Reimplement the main method using a switch and complete the pending steps. TODO
